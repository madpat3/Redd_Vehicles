	
	
	class RenderTargets
	{
		
		//Driver center
		class Mirror1
		{

			renderTarget = "rendertarget0";
			
			class CameraView1
			{

				pointPosition		= "d_center_pos";
				pointDirection		= "d_center_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
						
			}; 	

		};
		
		//Driver left 
		class Mirror2
		{

			renderTarget = "rendertarget1";

			class CameraView1
			{

				pointPosition		= "d_left_pos";
				pointDirection		= "d_left_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver Right
		class Mirror3
		{

			renderTarget = "rendertarget2";

			class CameraView1
			{

				pointPosition		= "d_right_pos";
				pointDirection		= "d_right_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};
		
		//Commander left
		class Mirror4
		{

			renderTarget = "rendertarget3"; 
			
			class CameraView1
			{

				pointPosition		= "c_left_pos";
				pointDirection		= "c_left_dir";
				renderQuality 		= 1;			
				renderVisionMode 	= 0;			
				fov 				= 0.7;	
						
			}; 	

		};

		//Commander right
		class Mirror5
		{

			renderTarget = "rendertarget4";

			class CameraView1
			{

				pointPosition		= "c_right_pos";
				pointDirection		= "c_right_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	

			}; 

		};

		class Mirror6
		{

			renderTarget = "rendertarget5";

			class CameraView1
			{

				pointPosition		= "gunnerview";
				pointDirection		= "gunnerviewDir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	

			}; 

		};

		//Mirror Left
		class Mirror7
		{

			renderTarget = "rendertarget6";

			class CameraView1
			{

				pointPosition		= "pip_left_mirror_pos";
				pointDirection		= "pip_left_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};

		//Mirror Right
		class Mirror8
		{

			renderTarget = "rendertarget7";

			class CameraView1
			{

				pointPosition		= "pip_right_mirror_pos";
				pointDirection		= "pip_right_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};
		
	};
