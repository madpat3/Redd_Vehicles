

	//triggered by BI eventhandler "turnOut"
	
	_veh = _this select 0;

	waitUntil {!isNull _veh};

	_unit = _this select 1;
	_turret = _this select 2;

	//checks if commander turns out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Marder_Commander_Up', false,true]; //resets variable for "climp up" function 
		_veh setVariable ['Redd_Marder_Commander_Winkelspiegel', false,true];//resets variable for "periscope mirrors" function 

	};