

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Wolf_Driver = "Redd_Tank_Wolf_Driver";
            Redd_Tank_Wolf_CoDriver = "Redd_Tank_Wolf_CoDriver";
            Redd_Tank_Wolf_Passenger1 = "Redd_Tank_Wolf_Passenger1";
            Redd_Tank_Wolf_Passenger2 = "Redd_Tank_Wolf_Passenger2";
            Redd_Tank_Wolf_Passenger_San = "Redd_Tank_Wolf_Passenger_San";

            KIA_Redd_Tank_Wolf_Driver = "KIA_Redd_Tank_Wolf_Driver";
            KIA_Redd_Tank_Wolf_CoDriver = "KIA_Redd_Tank_Wolf_CoDriver";
            KIA_Redd_Tank_Wolf_Passenger1 = "KIA_Redd_Tank_Wolf_Passenger1";
            KIA_Redd_Tank_Wolf_Passenger2 = "KIA_Redd_Tank_Wolf_Passenger2";
            KIA_Redd_Tank_Wolf_Passenger_San = "KIA_Redd_Tank_Wolf_Passenger_San";
            
        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Wolf_Driver: Crew
            {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\Redd_Tank_Wolf_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wolf_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wolf_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Wolf_Driver: DefaultDie
		    {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\KIA_Redd_Tank_Wolf_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wolf_CoDriver: Crew
            {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\Redd_Tank_Wolf_CoDriver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wolf_CoDriver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wolf_CoDriver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Wolf_CoDriver: DefaultDie
		    {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\KIA_Redd_Tank_Wolf_CoDriver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wolf_Passenger1: Crew
            {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\Redd_Tank_Wolf_Passenger1.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wolf_Passenger1",1};
                ConnectTo[]={"KIA_Redd_Tank_Wolf_Passenger1", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Wolf_Passenger1: DefaultDie
		    {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\KIA_Redd_Tank_Wolf_Passenger1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wolf_Passenger2: Crew
            {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\Redd_Tank_Wolf_Passenger2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wolf_Passenger2",1};
                ConnectTo[]={"KIA_Redd_Tank_Wolf_Passenger2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Wolf_Passenger2: DefaultDie
		    {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\KIA_Redd_Tank_Wolf_Passenger2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wolf_Passenger_San: Crew
            {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\Redd_Tank_Wolf_Passenger_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wolf_Passenger_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Wolf_Passenger_San", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Wolf_Passenger_San: DefaultDie
		    {

                file = "\Redd_Tank_LKW_leicht_gl_Wolf\anims\KIA_Redd_Tank_Wolf_Passenger_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            
        };

    };