

	//Triggerd by BI eventhandler "getOut"
	
	params ["_vehicle", "_role", "_unit", "_turret"];

	waitUntil {!isNull _vehicle};

	if ((_vehicle getVariable 'has_camonet_large') or (_vehicle getVariable 'has_camonet')) then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");

		_unit setUnitTrait ["camouflageCoef", _camouflage];

	};