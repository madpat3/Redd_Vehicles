

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Checks fuel amount and prevents refueling while wolf has large camo net
	//			 
	//	Example: ;
	//						 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  1: BOLLEAN - Fuelsate
	//				  2:  ? - Eventhandler
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_vehicle", "_fuelState","_thisEventHandler"];

	if (_fuelState) then
	{

		if (_vehicle getVariable 'has_camonet_large') then
		{

			_vehicle setFuel 0;

		}
		else
		{
			
			_vehicle removeEventHandler ["Fuel", _thisEventHandler];
			_fuel = _vehicle getVariable 'redd_wolf_fuel';
			_vehicle setFuel _fuel;

		};

	};

	true