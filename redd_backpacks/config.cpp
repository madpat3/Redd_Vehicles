

    class CfgPatches
	{
		
		class Redd_Bags
		{
			
			units[] = 
			{

                //temp
               "Redd_Milan_Static_Bag",
               "Redd_Milan_Static_Tripod",

				"Redd_Milan_Static_Barrel",
				"Redd_Milan_Static_Tripod",

				"Redd_Tank_M120_Tampella_Barrel",
				"Redd_Tank_M120_Tampella_Tripod"

			};

			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"Redd_Vehicles_Main"};
			
		};
		
	};

    class CfgVehicles
    {

        class Bag_Base;

        class Weapon_Bag_Base: Bag_Base
        {

            class assembleInfo{};

        };

        class Redd_Milan_Static_Barrel: Weapon_Bag_Base
		{

			faction = "BLU_F";
			author="Redd";
			editorCategory = "EdCat_Equipment";
			editorSubcategory = "EdSubcat_DismantledWeapons";
			hiddenSelectionsTextures[]={};
			mass=380;
            scope = 2;
			displayName = "$STR_Redd_Milan_Tube";
			model = "\Redd_Backpacks\Redd_Milan_Static_Barrel.p3d";
			editorPreview="\Redd_Backpacks\pictures\Milan_Bag_Pre_Picture.paa";
			picture = "\Redd_Backpacks\pictures\redd_milan_barrel_ui_pre_ca.paa";

			class assembleInfo: assembleInfo
			{

				displayName = "$STR_Redd_Milan";
				assembleTo="Redd_Milan_Static";
				base[] = {"Redd_Milan_Static_Tripod"};

			};
			
		};

		class Redd_Milan_Static_Tripod: Bag_Base 
		{

			author="Redd";
			faction = "BLU_F";
			editorCategory="EdCat_Equipment";
			editorSubcategory="EdSubcat_DismantledWeapons";
			hiddenSelectionsTextures[]={};
			icon="iconBackpack";
			mass=180;
			maximumLoad=0;
            scope = 2;
			displayName = "$STR_Redd_Milan_Tripod";
			model = "\Redd_Backpacks\Redd_Milan_Static_Tripod.p3d";
			editorPreview="\Redd_Backpacks\pictures\Milan_Tripod_Pre_Picture.paa";
			picture = "\Redd_Backpacks\pictures\redd_milan_tripod_ui_pre_ca.paa";

			class assembleInfo
			{

				primary=0;
				base="";
				assembleTo="";
				dissasembleTo[]={};
				displayName="";

			};

		};
            
        class Redd_Tank_M120_Tampella_Barrel: Weapon_Bag_Base
        {

            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=380;
            scope = 2;
            displayName = "$STR_Redd_m120_barrel";
            model = "\Redd_Backpacks\Redd_Tank_M120_Tampella_Barrel.p3d";
            editorPreview="\Redd_Backpacks\pictures\m_120_barrel_pre.paa";
            picture = "\Redd_Backpacks\pictures\redd_m120_barrel_ui_pre_ca.paa";

            class assembleInfo: assembleInfo 
            {

                displayName = "M120 Tampella";
                assembleTo="Redd_Tank_M120_Tampella";
                base[] = {"Redd_Tank_M120_Tampella_Tripod"};

            };

        };

        class Redd_Tank_M120_Tampella_Tripod: Bag_Base 
        {

            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_Redd_m120_base";
            model = "\Redd_Backpacks\Redd_Tank_M120_Tampella_Tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\m_120_Tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\redd_m120_tripod_ui_pre_ca.paa";

            class assembleInfo
            {
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };

        };

    };
    