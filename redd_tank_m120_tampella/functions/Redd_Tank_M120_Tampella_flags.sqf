

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Sets flags
	//			 
	//	Example: ;
	//			
	//			 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  1: NUMBER - 1,2,3 (1=Red,2=Green,3=Blue)
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_vehicle","_color"];

	if (_vehicle getVariable 'has_flag') then
	{

		_vehicle forceFlagTexture "";
		_vehicle setVariable ['has_flag', false,true];

	}
	else
	{

		if (_color == 1) then
		{

			_vehicle forceFlagTexture "\A3\Data_F\Flags\Flag_red_CO.paa";
			_vehicle setVariable ['has_flag', true,true];

		};

		if (_color == 2) then
		{

			_vehicle forceFlagTexture "\A3\Data_F\Flags\Flag_green_CO.paa";
			_vehicle setVariable ['has_flag', true,true];

		};

		if (_color == 3) then
		{

			_vehicle forceFlagTexture "\A3\Data_F\Flags\Flag_blue_CO.paa";
			_vehicle setVariable ['has_flag', true,true];

		};

	};