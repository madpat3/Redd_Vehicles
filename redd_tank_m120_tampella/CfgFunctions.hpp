
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class m120_init
                {

                    file = "\Redd_Tank_M120_Tampella\functions\Redd_Tank_M120_Tampella_init.sqf";

                };

                class m120_fired
                {

                    file = "\Redd_Tank_M120_Tampella\functions\Redd_Tank_M120_Tampella_fired.sqf";

                };

                class m120_flags
                {

                    file = "\Redd_Tank_M120_Tampella\functions\Redd_Tank_M120_Tampella_flags.sqf";

                };

                class m120_flares
                {

                    file = "\Redd_Tank_M120_Tampella\functions\Redd_Tank_M120_Tampella_flares.sqf";

                };
                
            };

        };

    };