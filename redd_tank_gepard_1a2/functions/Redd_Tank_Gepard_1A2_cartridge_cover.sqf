

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Animates cartridge cover
	//			 
	//	Example: ;
	//				 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_veh"];

	waitUntil {!(alive _veh) or (inputAction "defaultAction" > 0)};

	if !(alive _veh) exitWith {};

	_veh animateSource ["Gurt_Deckel_source", 1];

	waitUntil {!(alive _veh) or (inputAction "defaultAction" < 1)};

	_veh animateSource ["Gurt_Deckel_source", 0];

	if !(alive _veh) exitWith {};

	[_veh] spawn Redd_fnc_Gepard_cartridge_cover;
