

	//Gepard init

	params ["_veh"];

	waitUntil {!isNull _veh};

	[_veh] spawn redd_fnc_Gepard_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Gepard_Bat_Komp;//spawns function to set battalion and company numbers

	_veh setVariable ['Redd_Gepard_Commander_Out', false,true];//initiates for commander turnout
	_veh setVariable ['Redd_Gepard_Commander_Up', false,true];//initiates varibale for "climp up" and "climp down" function of turned out commander
	_veh setVariable ['Redd_Gepard_Gunner_Out', false,true];//initiates for gunner turnout
	_veh setVariable ['Redd_Gepard_Gunner_Up', false,true];//initiates varibale for "climp up" and "climp down" function of turned out gunner
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags

	[_veh] spawn Redd_fnc_Gepard_Radar;
	[_veh] spawn Redd_fnc_Gepard_cartridge_cover;

	