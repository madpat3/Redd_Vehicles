

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Gepard_Driver = "Redd_Gepard_Driver";
            Redd_Gepard_Driver_Out = "Redd_Gepard_Driver_Out";
            Redd_Gepard_Commander = "Redd_Gepard_Commander";
            Redd_Gepard_Commander_Out = "Redd_Gepard_Commander_Out";
            Redd_Gepard_Commander_Out_Low = "Redd_Gepard_Commander_Out_Low";
            Redd_Gepard_Gunner = "Redd_Gepard_Gunner";
            Redd_Gepard_Gunner_Out = "Redd_Gepard_Gunner_Out";
            Redd_Gepard_Gunner_Out_Low = "Redd_Gepard_Gunner_Out_Low";

            KIA_Redd_Gepard_Driver = "KIA_Redd_Gepard_Driver";
            KIA_Redd_Gepard_Driver_Out = "KIA_Redd_Gepard_Driver_Out";
            KIA_Redd_Gepard_Commander = "KIA_Redd_Gepard_Commander";
            KIA_Redd_Gepard_Commander_Out = "KIA_Redd_Gepard_Commander_Out";
            KIA_Redd_Gepard_Commander_Out_Low = "KIA_Redd_Gepard_Commander_Out_Low";
            KIA_Redd_Gepard_Gunner = "KIA_Redd_Gepard_Gunner";
            KIA_Redd_Gepard_Gunner_Out = "KIA_Redd_Gepard_Gunner_Out";
            KIA_Redd_Gepard_Gunner_Out_Low = "KIA_Redd_Gepard_Gunner_Out_Low";

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            //Driver
            class Redd_Gepard_Driver: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Driver",1};
                ConnectTo[]={"KIA_Redd_Gepard_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Gepard_Driver_Out: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Commander
            class Redd_Gepard_Commander: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Gepard_Commander_Out: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Commander_Out: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Gepard_Commander_Out_Low: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander_Out_Low.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander_Out_Low",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander_Out_Low", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Commander_Out_Low: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Commander_Out_Low.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Gunner
            class Redd_Gepard_Gunner: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Gunner.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Gunner",1};
                ConnectTo[]={"KIA_Redd_Gepard_Gunner", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Gunner: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Gunner.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Gepard_Gunner_Out: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Gunner_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Gunner_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Gunner_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Gunner_Out: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Gunner_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Gepard_Gunner_Out_Low: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Gunner_Out_Low.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Gunner_Out_Low",1};
                ConnectTo[]={"KIA_Redd_Gepard_Gunner_Out_Low", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Gepard_Gunner_Out_Low: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Gunner_Out_Low.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

        };

    };