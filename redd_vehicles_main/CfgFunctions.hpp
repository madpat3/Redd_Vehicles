

    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Milan_Fired
                {

                    file = "\Redd_Vehicles_Main\functions\Redd_Milan_Fired.sqf";

                };

                class Delete_Tube
                {

                    file = "\Redd_Vehicles_Main\functions\Redd_Delete_Tube.sqf";

                };

                class Main_Plate
                {

                    file = "\Redd_Vehicles_Main\functions\Redd_Main_plate.sqf";

                };

                class SmokeLauncher
                {

                    file = "\Redd_Vehicles_Main\functions\Redd_smokeLauncher.sqf";

                };

            };

        };

    };