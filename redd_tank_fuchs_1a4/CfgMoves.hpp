

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Fuchs_Driver = "Redd_Tank_Fuchs_Driver";
            Redd_Tank_Fuchs_Co_Driver_TurnedIn = "Redd_Tank_Fuchs_Co_Driver_TurnedIn";
            Redd_Tank_Fuchs_Co_Driver_TurnedOut = "Redd_Tank_Fuchs_Co_Driver_TurnedOut";
            Redd_Tank_Fuchs_Co_Driver_TurnedOut_2 = "Redd_Tank_Fuchs_Co_Driver_TurnedOut_2";
            Redd_Tank_Fuchs_Passenger_1 = "Redd_Tank_Fuchs_Passenger_1";
            Redd_Tank_Fuchs_Passenger_1_1 = "Redd_Tank_Fuchs_Passenger_1_1";
            Redd_Tank_Fuchs_Passenger_1_2 = "Redd_Tank_Fuchs_Passenger_1_2";
            Redd_Tank_Fuchs_Passenger_2 = "Redd_Tank_Fuchs_Passenger_2";
            Redd_Tank_Fuchs_Passenger_3 = "Redd_Tank_Fuchs_Passenger_3";
            Redd_Tank_Fuchs_Passenger_4 = "Redd_Tank_Fuchs_Passenger_4";
            Redd_Tank_Fuchs_Milan = "Redd_Tank_Fuchs_Milan";
            Redd_Tank_Fuchs_Milan_2 = "Redd_Tank_Fuchs_Milan_2";
            Redd_Tank_Fuchs_Pi = "Redd_Tank_Fuchs_Pi";
            Redd_Tank_Fuchs_Pi_In = "Redd_Tank_Fuchs_Pi_In";
            Redd_Tank_Fuchs_Passenger_1_San = "Redd_Tank_Fuchs_Passenger_1_San";
            Redd_Tank_Fuchs_Passenger_1_San_TurnOut = "Redd_Tank_Fuchs_Passenger_1_San_TurnOut";
            Redd_Tank_Fuchs_Passenger_1_1_San = "Redd_Tank_Fuchs_Passenger_1_1_San";
            Redd_Tank_Fuchs_Passenger_1_2_San = "Redd_Tank_Fuchs_Passenger_1_2_San";
            Redd_Tank_Fuchs_Passenger_2_San = "Redd_Tank_Fuchs_Passenger_2_San";
            Redd_Tank_Fuchs_Passenger_3_San = "Redd_Tank_Fuchs_Passenger_3_San";

            KIA_Redd_Tank_Fuchs_Driver = "KIA_Redd_Tank_Fuchs_Driver";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut";
            KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2 = "KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2";
            KIA_Redd_Tank_Fuchs_Passenger_1 = "KIA_Redd_Tank_Fuchs_Passenger_1";
            KIA_Redd_Tank_Fuchs_Passenger_1_1 = "KIA_Redd_Tank_Fuchs_Passenger_1_1";
            KIA_Redd_Tank_Fuchs_Passenger_1_2 = "KIA_Redd_Tank_Fuchs_Passenger_1_2";
            KIA_Redd_Tank_Fuchs_Passenger_2 = "KIA_Redd_Tank_Fuchs_Passenger_2";
            KIA_Redd_Tank_Fuchs_Passenger_3 = "KIA_Redd_Tank_Fuchs_Passenger_3";
            KIA_Redd_Tank_Fuchs_Passenger_4 = "KIA_Redd_Tank_Fuchs_Passenger_4";
            KIA_Redd_Tank_Fuchs_Milan = "KIA_Redd_Tank_Fuchs_Milan";
            KIA_Redd_Tank_Fuchs_Milan_2 = "KIA_Redd_Tank_Fuchs_Milan_2";
            KIA_Redd_Tank_Fuchs_Pi = "KIA_Redd_Tank_Fuchs_Pi";
            KIA_Redd_Tank_Fuchs_Pi_2 = "KIA_Redd_Tank_Fuchs_Pi_2";
            KIA_Redd_Tank_Fuchs_Passenger_1_San = "KIA_Redd_Tank_Fuchs_Passenger_1_San";
            KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut = "KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut";
            KIA_Redd_Tank_Fuchs_Passenger_1_1_San = "KIA_Redd_Tank_Fuchs_Passenger_1_1_San";
            KIA_Redd_Tank_Fuchs_Passenger_1_2_San = "KIA_Redd_Tank_Fuchs_Passenger_1_2_San";
            KIA_Redd_Tank_Fuchs_Passenger_2_San = "KIA_Redd_Tank_Fuchs_Passenger_2_San";
            KIA_Redd_Tank_Fuchs_Passenger_3_San = "KIA_Redd_Tank_Fuchs_Passenger_3_San";

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Fuchs_Driver: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            
            class Redd_Tank_Fuchs_Co_Driver_TurnedIn: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedIn.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedIn.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Co_Driver_TurnedOut: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedOut.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Co_Driver_TurnedOut_2: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Co_Driver_TurnedOut_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Co_Driver_TurnedOut_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_1.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_1",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_1", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_1: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_1_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_2", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_2", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_3: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_3.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_3",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_3", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_3: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_3.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Passenger_4: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_4.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_4",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_4", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_4: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_4.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
            class Redd_Tank_Fuchs_Milan: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Milan.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Milan",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Milan", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Milan: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Milan.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Milan_2: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Milan_2.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Milan_2",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Milan_2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Milan_2: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Milan_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Pi: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Pi.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Pi",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Pi", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Pi: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Pi.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Pi_In: Crew
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Pi_In.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Pi_In",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Pi_In", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_Redd_Tank_Fuchs_Pi_In: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Pi_In.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_San_TurnOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_San_TurnOut.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_San_TurnOut.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_1_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_1_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_1_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_1_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_1_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_1_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_1_2_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_1_2_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_1_2_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_1_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_1_2_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_2_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_2_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_2_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_2_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_2_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };

            class Redd_Tank_Fuchs_Passenger_3_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\Redd_Tank_Fuchs_Passenger_3_San.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Fuchs_Passenger_3_San",1};
                ConnectTo[]={"KIA_Redd_Tank_Fuchs_Passenger_3_San", 1};

            };
            
            class KIA_Redd_Tank_Fuchs_Passenger_3_San: Crew 
            {

                file = "\Redd_Tank_Fuchs_1A4\anims\KIA_Redd_Tank_Fuchs_Passenger_3_San.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

            };
            
        };

    };