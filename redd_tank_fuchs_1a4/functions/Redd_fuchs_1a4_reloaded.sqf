

	//triggerd by BI eventhandler "reload" when marder reloads weapon

	params ["_veh","_weap"];
	
	waitUntil {!isNull _veh};
	
	//checks if weapon is milan
	if (_weap == "Redd_Milan") then  
	{
	
		[_veh] spawn redd_fnc_Milan_Reload;//spawns function to animate milan empty tube drop and reloading
		
	};