    

    class CfgVehicles
	{
		class Car;

		class Car_F: Car
		{

			class HitPoints;
			class Turrets;

		};

		class Truck_F: Car_F
		{

			class HitPoints: HitPoints
			{

				class HitRGlass;
				class HitLGlass;
				class HitGlass1;
				class HitGlass2;
				class HitGlass3;
				class HitGlass4;
				class HitGlass5;
				class HitBody;
				class HitFuel;
				class HitLFWheel;
				class HitLBWheel;
				class HitLMWheel;
				class HitLF2Wheel;
				class HitRFWheel;
				class HitRBWheel;
				class HitRMWheel;
				class HitRF2Wheel;
				class HitEngine;

			};

			class EventHandlers;
			class AnimationSources;

			class Turrets: Turrets
			{

				class MainTurret;
				class ViewGunner;

			};

			class ViewPilot;
		
		};

        class Redd_Tank_LKW_5t_mil_gl_KAT_I_Base: Truck_F
	    {
            
           	//#include "Sounds.hpp"
			//#include "PhysX.hpp"
			//#include "Pip.hpp"
			
            displayName = "STR_LKW_5t_mil_gl_KAT_I";
			icon = "\a3\soft_f_beta\Truck_01\Data\UI\map_truck_01_CA.paa";
			picture="\a3\soft_f_beta\Truck_01\Data\UI\Truck_01_Base_CA.paa";
			side = 1;
			crew = "B_Soldier_F";
            author = "Tank, Redd";
            model = "\Redd_Tank_LKW_5t_mil_gl_KAT_I\Redd_Tank_LKW_5t_mil_gl_KAT_I";
            editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Trucks";
            getInAction = "GetInMedium";
		    getOutAction = "GetOutMedium"; 
            driverAction = "Redd_Tank_Fuchs_Driver";//ToDo
            driverInAction="Redd_Tank_Fuchs_Driver";//ToDo
            armor = 200;
			dustFrontLeftPos = "TrackFLL";
			dustFrontRightPos = "TrackFRR";
			dustBackLeftPos = "TrackBLL";
			dustBackRightPos = "TrackBRR";
            viewDriverInExternal = 1;
            lodTurnedIn = 1100; //Pilot 1100
			lodTurnedOut = 1100; //Pilot 1100
            enableManualFire = 0;
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos cargo";
			headGforceLeaningFactor[]={0.00075,0,0.0075};
			
            
		    
            weapons[] = {"TruckHorn"};
			destrType = "DestructWreck";
			wheelDamageThreshold = 1;//ToDo
        	wheelDestroyThreshold = 0.99;//ToDo
        	wheelDamageRadiusCoef = 1;//ToDo
        	wheelDestroyRadiusCoef = 0.55;//ToDo
			typicalCargo[] = {"B_Soldier_F"};
			transportSoldier = 5;//ToDo
			cargoAction[] = {"Redd_Tank_Fuchs_Passenger_2","Redd_Tank_Fuchs_Passenger_3","Redd_Tank_Fuchs_Passenger_4"};//ToDo
            cargoProxyIndexes[] = {2,3,4,5,6};//ToDo
			viewCargoInExternal = 1;
			cargoCompartments[] = {"Compartment2"};
			memoryPointTrackFLL = "TrackFLL";//ToDo
		    memoryPointTrackFLR = "TrackFLR";//ToDo
		    memoryPointTrackBLL = "TrackBLL";//ToDo
		    memoryPointTrackBLR = "TrackBLR";//ToDo
		    memoryPointTrackFRL = "TrackFRL";//ToDo
		    memoryPointTrackFRR = "TrackFRR";//ToDo
		    memoryPointTrackBRL = "TrackBRL";//ToDo
		    memoryPointTrackBRR = "TrackBRR";//ToDo
            
			TFAR_AdditionalLR_Turret[] = {{0,3},{1}};//ToDo
			/* //ToDo
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1}}};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					allowedPositions[] = {"driver", "commander", "gunner",{"ffv",{0,3}},{"Turret",{1}}};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 0;
			//Ende ACRE2
			*/
            hiddenSelections[] = 
            {
                
                
				
            };
			
            class PlayerSteeringCoefficients //ToDo
		    {

			    turnIncreaseConst = 0.1;
			    turnIncreaseLinear = 1;
			    turnIncreaseTime = 1;

			    turnDecreaseConst = 1;
			    turnDecreaseLinear = 1; 
			    turnDecreaseTime = 0;

			    maxTurnHundred = 0.7;
				
            };

			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{
					
					

				};
				
        	};

            class Exhausts
		    {

			    class Exhaust1
			    {

				    position = "exhaust1_pos"; 
				    direction = "exhaust1_dir";
				    effect = "ExhaustsEffect";

			    };

		    };

            class Reflectors
		    {

                class Left
                {

                    color[] = {1900, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare = 1;
                    dayLight = 1;
                    flareSize = 1;

                    class Attenuation
                    {

                        start = 1;
                        constant = 0;
                        linear = 0;
                        quadratic = 0.25;
                        hardLimitStart = 30;
                        hardLimitEnd = 60;
                   
                    };

                };

                class Right: Left
			    {

                    position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";

			    };
				
			};

			class HitPoints: HitPoints//ToDo
			{

				class HitFuel: HitFuel
				{

					armor=0.5;
					passThrough=0;
					minimalHit=0;
					explosionShielding=1.5;
					radius=0.44999999;

				};

				class HitEngine: HitEngine
				{

					armor=0.5;
					passThrough=0;
					minimalHit=0.1;
					explosionShielding=0.5;
					radius=0.44999999;

				};

				class HitBody: HitBody
				{

					armor=1;
					passThrough=0;
					minimalHit=0;
					explosionShielding=1.5;
					radius=0.33000001;

				};

				class HitLFWheel: HitLFWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitLBWheel: HitLBWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitLMWheel: HitLMWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitLF2Wheel: HitLF2Wheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitRFWheel: HitRFWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitRBWheel: HitRBWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitRMWheel: HitRMWheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitRF2Wheel: HitRF2Wheel
				{

					armor=0.5;
					minimalHit=0.02;
					passThrough=0;
					explosionShielding=4;
					radius=0.33000001;

				};

				class HitGlass1: HitGlass1
				{

					armor=1.5;
					passThrough=0;
					explosionShielding=3;
					radius=0.33000001;

				};

				class HitGlass2: HitGlass2
				{

					armor=1.5;
					passThrough=0;
					explosionShielding=3;
					radius=0.33000001;

				};

				class HitGlass3: HitGlass3
				{

					armor=1.5;
					passThrough=0;
					explosionShielding=3;
					radius=0.33000001;

				};

				class HitGlass4: HitGlass4
				{

					armor=1.5;
					passThrough=0;
					explosionShielding=3;
					radius=0.33000001;

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};

            class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};

            class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};
			/*
            class MFD 
		    {

			    class ClockHUD
			    {

			        #include "cfgHUD.hpp"

			    };
		    };
            */
            class Turrets: Turrets 
            {
			
                class MainTurret: MainTurret 
				{

                    class Turrets: Turrets 
					{

						

					};
					
                };
				
            };

            class AnimationSources
		    {

				

		    };

            class UserActions
		    {


		    };
			
            class EventHandlers: EventHandlers 
            {

               

            };

			class Attributes 
			{



			};

        };

        //Fuchs_1A4_JgGrp
        class Redd_Tank_LKW_5t_mil_gl_KAT_I_Flecktarn: Redd_Tank_LKW_5t_mil_gl_KAT_I_Base
		{
			
			editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_JG_W.paa";//ToDo
			scope=2;
            scopeCurator = 2;
			displayName="STR_Redd_Tank_LKW_5t_mil_gl_KAT_I_Flecktarn";

            hiddenSelectionsTextures[] = 
            {

                

            };

			class textureSources
			{

				class Fleck
				{

					displayName = "STR_LKW_5t_mil_gl_KAT_I_Flecktarn";
					author = "ReddNTank";

					textures[]=
					{

						

					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "STR_LKW_5t_mil_gl_KAT_I_Tropentarn";
					author = "ReddNTank";

					textures[]=
					{

						
					
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "STR_LKW_5t_mil_gl_KAT_I_Wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{

						

					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};

            class AnimationSources: AnimationSources
		    {

                

            };

		};

    };