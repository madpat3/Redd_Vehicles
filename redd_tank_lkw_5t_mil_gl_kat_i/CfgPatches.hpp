

    class CfgPatches
	{
		
		class Redd_Tank_LKW_5t_mil_gl_KAT_I
		{
			
			units[] = 
			{
				
				"Redd_Tank_LKW_5t_mil_gl_KAT_I_Flecktarn",
				"Redd_Tank_LKW_5t_mil_gl_KAT_I_Tropentarn",
				"Redd_Tank_LKW_5t_mil_gl_KAT_I_Wintertarn"

			};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Soft_F"};

		};
		
	};