

	//Triggerd by BI eventhandler "getout"
	
	_veh = _this select 0;
	
	_pos = _this select 1;
	_unit = _this select 2;
	_turret = _this select 3;

	//TOW
	if (_turret isEqualTo [0]) then
	{

		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];
		_veh animate ['Seat_L_Trans', 0];
		_veh animate ['TOW_Hide_Rohr', 1];

	};

	//MG3
	if (_turret isEqualTo [1]) then
	{

		[_veh,[[0,1],false]] remoteExecCall ['lockTurret'];
		_veh animate ['Seat_R_Trans', 0];

	};