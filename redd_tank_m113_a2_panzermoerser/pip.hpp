	
	
	class RenderTargets
	{
		
		//Left Mirror
		class Mirror1
		{

			renderTarget = "rendertarget0";
			
			class CameraView1
			{

				pointPosition		= "pip_left_mirror_pos";
				pointDirection		= "pip_left_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
						
			}; 	

		};
		
		//Right Mirror
		class Mirror2
		{

			renderTarget = "rendertarget1";

			class CameraView1
			{

				pointPosition		= "pip_right_mirror_pos";
				pointDirection		= "pip_right_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Turret Left Mirror
		class Mirror3
		{

			renderTarget = "rendertarget2";

			class CameraView1
			{

				pointPosition		= "pip_t_left_mirror_pos";
				pointDirection		= "pip_t_left_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Turret Front Mirror
		class Mirror4
		{

			renderTarget = "rendertarget3";

			class CameraView1
			{

				pointPosition		= "pip_t_front_mirror_pos";
				pointDirection		= "pip_t_front_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Turret Right Mirror
		class Mirror5
		{

			renderTarget = "rendertarget4";

			class CameraView1
			{

				pointPosition		= "pip_t_right_mirror_pos";
				pointDirection		= "pip_t_right_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver Left Mirror
		class Mirror6
		{

			renderTarget = "rendertarget5";

			class CameraView1
			{

				pointPosition		= "pip_d_left_mirror_pos";
				pointDirection		= "pip_d_left_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver Front Mirror
		class Mirror7
		{

			renderTarget = "rendertarget6";

			class CameraView1
			{

				pointPosition		= "pip_d_front_mirror_pos";
				pointDirection		= "pip_d_front_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver Front Mirror
		class Mirror8
		{

			renderTarget = "rendertarget7";

			class CameraView1
			{

				pointPosition		= "pip_d_right_mirror_pos";
				pointDirection		= "pip_d_right_mirror_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

	};
