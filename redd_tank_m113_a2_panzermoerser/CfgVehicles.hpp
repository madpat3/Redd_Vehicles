

	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;

		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/
		class redd_tank_m113_a2_panzermoerser_base: Tank_F
		{
			
			//#include "Sounds.hpp" //ToDo
			//#include "PhysX.hpp" //ToDo
			//#include "Pip.hpp" //ToDo
			
			displayname = "redd_tank_m113_a2_panzermoerser_base";
			picture=""; //ToDo
			icon=""; //ToDo
			side = 1;
			crew = "B_crew_F";
			author = "ReddNTank";
			model = "\redd_tank_m113_a2_panzermoerser\redd_tank_m113_a2_panzermoerser.p3d";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Waffentraeger";
			smokeLauncherGrenadeCount = 8;
			smokeLauncherVelocity = 14;
			smokeLauncherOnTurret = 0;
			smokeLauncherAngle = 142.5;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="";//ToDo
			driverInAction="";//ToDo
			armor = 200;
			armorStructural = 5;
			cost = 1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_2_bound";
			dustFrontRightPos = "wheel_2_2_bound";
			dustBackLeftPos = "wheel_1_6_bound";
			dustBackRightPos = "wheel_2_6_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = "31+32+14";
			driverCanSee = "31+32+14";
			gunnerCanSee = "31+32+14";
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos driver";
			forceHideDriver = 0;
			driverWeaponsInfoType="Redd_RCS_Driver";
			headGforceLeaningFactor[]={0.00075,0,0.0075};
			
			//ToDo
			/*
			//ACRE2
			class AcreRacks
			{

				class Rack_1 
				{

					displayName = "Zugkreis";
					shortName = "ZgKr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};
				
				class Rack_2 
				{

					displayName = "Kompaniekreis";
					shortName = "Kmpkr";
					componentName = ACRE_SEM90;
					allowedPositions[] = {"driver","commander","gunner"};
					disabledPositions[] = {};
					defaultComponents[] = {};
					mountedRadio = ACRE_SEM70;
					isRadioRemovable = 0;
					intercom[] = {};

				};

			};

			class AcreIntercoms 
			{

				class Intercom_1 
				{

					displayName = "Bordverständigung";
					shortName = "Bv";
					allowedPositions[] = {"driver", "commander", "gunner"};
					disabledPositions[] = {};
					limitedPositions[] = {};
					numLimitedPositions = 0;
					connectedByDefault = 1;

				};

			};

			acre_hasInfantryPhone = 1;
			acre_infantryPhoneDisableRinging = 0;
			acre_infantryPhoneCustomRinging[] = {};
			acre_infantryPhoneIntercom[] = {"all"};
			acre_infantryPhoneControlActions[] = {"all"};
			acre_eventInfantryPhone = "QFUNC(noApiFunction)";
			//Ende ACRE2
			*/

			//ToDo
			hiddenSelections[] = 
			{
				
				
				
			};

			//ToDo
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					

				};
				
        	};

			class ViewOptics: ViewOptics 
			{
				
				initFov = 0.75;
				maxFov = 0.75;
				minFov = 0.75;
				visionMode[] = {"Normal","NVG"};
				
			};

			//ToDo
			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust 1";
					direction = "exhaust_dir 1";
					effect = "ExhaustEffectTankBack";
					
				};
				
			};

			class Reflectors 
			{

				class Left 
				{
					
					color[] = {1900, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="hull";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.3;
					armorComponent="";

				};

				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="DamageVisual";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.21;
					armorComponent="";

				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="engine";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.3;
					armorComponent="";

				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";

				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					armorComponent="";

				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};
				
			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

			//ToDo
			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{
						
						class CommanderOptics: CommanderOptics //[0,0] Commander
						{
						
							gunnerForceOptics=0;
							gunnerAction = "";//ToDo
							gunnerInAction="";//ToDo
							initElev = 0;
							minElev = -10;
							maxElev = 90;
							initTurn = 0;
							minTurn = -360;
							maxTurn = 360;
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							startEngine = 0;
							gunnerGetInAction="GetInMedium";
							gunnerGetOutAction="GetOutMedium";
							turretInfoType = "";//ToDo
							stabilizedInAxes = 3;
							lodTurnedIn = 1100; //Pilot
							lodTurnedOut = 1100; //Pilot
							soundAttenuationTurret = "TankAttenuation";
							gunnerCompartments= "Compartment1";
							weapons[] = {"Redd_SmokeLauncher"};
							magazines[] = {"Redd_SmokeLauncherMag"};
							ace_fcs_Enabled = 0;
							gunnerOutOpticsModel="";
							outGunnerMayFire=0;
							soundServo[]={};
							soundServoVertical[]={};

							class ViewOptics: ViewOptics
							{
								
								visionMode[] = {"Normal", "NVG", "TI"};
								thermalMode[] = {2,3};
								initFov = 0.4;
								minFov = 0.08;
								maxFov = 0.03;
								
							};
							
							class OpticsIn
							{

								class Day1
								{
									
									initAngleX = 0;
									minAngleX = -30;
									maxAngleX = 30;
									initAngleY = 0;
									minAngleY = -100;
									maxAngleY = 100;
									initFov = 0.27;
									maxFov = 0.27;
									minFov = 0.27;
									visionMode[] = {"Normal", "NVG"};
									thermalMode[] = {2,3};
									gunnerOpticsModel = "";//ToDo
									gunnerOpticsEffect[] = {};
												
								};
								
							};

							class HitPoints 
							{
								
								class HitTurret	
								{
									
									armor = 1;
									material = -1;
									name = "vezVelitele";
									visual="DamageVisual";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.09;
									armorComponent="";
									isTurret=1;

								};
								
								class HitGun	
								{
									
									armor = 1;
									material = -1;
									name = "zbranVelitele";
									visual="DamageVisual";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.09;
									armorComponent="";
									isGun=1;

								};
								
							};
							
							
						};

						class MG_Turret: NewTurret
						{};

					};

					//Mainturret [0]
					weapons[] =
					{
						
						//ToDo

					};

					magazines[] =
					{
						
						//ToDo

					};
					
					startEngine = 0;
					stabilizedInAxes = 3;
					forceHideGunner = 0;
					initElev = 0;
					minElev = -10;//ToDo
					maxElev = 90;//ToDo
					initTurn = 0;//ToDo
					minTurn = -360;//ToDo
					maxTurn = 360;//ToDo
					maxHorizontalRotSpeed = 3.2;// 1 = 45°/sec //ToDo
					maxVerticalRotSpeed = 1.33; //ToDo
					gunnerForceOptics=0;
					gunnerInAction="";//ToDo
					gunnerAction="";//ToDo
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					gunnerGetInAction="GetInMedium";
					gunnerGetOutAction="GetOutMedium";
					turretInfoType = ""; //ToDo
					discreteDistance[] = {100};//ToDo
					discreteDistanceInitIndex = 0;//ToDo
					lodTurnedIn = 1100; //Pilot
					lodTurnedOut = 1100; //Pilot
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment1";
					lockWhenDriverOut = 0;
					ace_fcs_Enabled = 0;
					gunnerOutOpticsModel="";
					soundServo[]={};
					soundServoVertical[]={};

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
	
					class OpticsIn //ToDo
					{

						class Day1
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.27;
							maxFov = 0.27;
							minFov = 0.27;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "";
							gunnerOpticsEffect[] = {};
										
						};
						
						class Day2
						{
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.06;
							maxFov = 0.06;
							minFov = 0.06;
							visionMode[] = {"Normal", "NVG"};
							thermalMode[] = {2,3};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Gepard";
							gunnerOpticsEffect[] = {};
							
						};
						
					};

					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "vez";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.43;
							armorComponent="";
							isTurret=1;

						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "zbran";
							visual="DamageVisual";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.20;
							armorComponent="";
							isGun=1;

						};
					
					};
					
				};

			};

			class AnimationSources
			{
				
				

			};

			class UserActions
			{

				
				
			};

			class EventHandlers: EventHandlers 
			{
				
				
				
			};

			class Attributes 
			{


			};

		};
		
		/*	Public class Flecktarn	*/
		class redd_tank_m113_a2_panzermoerser_flecktarn: redd_tank_m113_a2_panzermoerser_base
		{
			
			editorPreview="";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_m113_a2_panzermoerser_flecktarn";

			hiddenSelectionsTextures[] = 
			{
				
				
			};
			
			class textureSources
			{

				class Fleck
				{

					displayName = "$STR_m113_a2_panzermoerser_flecktarn";
					author = "ReddNTank";

					textures[]=
					{
						
						
						
					};

					factions[] = {"BLU_F"};

				};

				class Tropen
				{

					displayName = "$STR_m113_a2_panzermoerser_tropentarn";
					author = "ReddNTank";

					textures[]=
					{
						
						
						
					};

					factions[] = {"BLU_F"};

				};

				class Winter
				{

					displayName = "$STR_m113_a2_panzermoerser_wintertarn";
					author = "ReddNTank";
					
					textures[]=
					{
						
						
					};

					factions[] = {"BLU_F"};

				};

			};

			textureList[]=
			{

				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0

			};
			
		};
		
		/*	Public class Tropentarn	*/
		class redd_tank_m113_a2_panzermoerser_tropentarn: redd_tank_m113_a2_panzermoerser_flecktarn
		{
			
			scopeArsenal = 0;
			editorPreview="";
			displayName="$STR_m113_a2_panzermoerser_tropentarn";

			hiddenSelectionsTextures[] = 
			{
				
				
				
			};
			
		};
		
		/*	Public class Wintertarn	*/
		class redd_tank_m113_a2_panzermoerser_wintertarn: redd_tank_m113_a2_panzermoerser_flecktarn 
		{
			
			scopeArsenal = 0;
			editorPreview="";
			displayName="$STR_m113_a2_panzermoerser_wintertarn";

			hiddenSelectionsTextures[] = 
			{
				
				
				
			};
			
		};
		
	};